#!/bin/bash -xe
echo '---------- Installing Nginx... ----------'
release=$(lsb_release -cs)
echo "deb https://nginx.org/packages/ubuntu/ $release nginx" > /etc/apt/sources.list.d/nginx.list
echo "deb-src https://nginx.org/packages/ubuntu/ $release nginx" >> /etc/apt/sources.list.d/nginx.list
wget http://nginx.org/packages/keys/nginx_signing.key
cat nginx_signing.key | sudo apt-key add -
apt-get update -q
apt-get install nginx=1.18.* -yq
apt-mark hold nginx=1.18.*
echo '---------- Installing certbot... ----------'
add-apt-repository ppa:certbot/certbot -y
apt-get install python-certbot-nginx -yq
echo '---------- Applying cloned Nginx config... ----------'
rsync -r -v -I lc-proxy-conf-test/* /etc/nginx
echo '---------- Creating symlinks... ----------'
mkdir /etc/nginx/sites-enabled
ln -s /etc/nginx/sites-available/debug.rest.conf /etc/nginx/sites-enabled/debug.rest.conf
echo '---------- Creating lets encrypt ACME-challenge dirs... ----------'
mkdir -p /var/www/_letsencrypt
# echo '---------- Creating dhparam.pem... ----------'
# aws ssm get-parameter --name /otvoreni-grad/proxy/DHPARAM --with-decryption --region eu-west-1 --query "Parameter.Value" --output text > /etc/nginx/dhparam.pem
echo '---------- Temporariliy commenting out SSL lines until we obtain certs... ----------'
sed -i -r 's/(listen .*443)/\1;#/g; s/(ssl_(certificate|certificate_key|trusted_certificate) )/#;#\1/g' /etc/nginx/sites-available/debug.rest.conf
echo '---------- Starting and enabling Nginx... ----------'
systemctl start nginx
systemctl enable nginx
echo '---------- Getting cert from letsencrypt... ----------'
[ ! -d '/etc/letsencrypt/live/debug.rest' ] && certbot certonly --webroot -d *.debug.rest --email tomislav.medanovic@gmail.com -w /var/www/_letsencrypt -n --agree-tos --force-renewal
echo '---------- Uncommenting Nginx SSL config... ----------'
sed -i -r 's/#?;#//g' /etc/nginx/sites-available/debug.rest.conf
echo '---------- Seting letsencrypt premissions... ----------'
chmod -R 755 /etc/letsencrypt/live/
chmod -R 755 /etc/letsencrypt/archive/
echo '---------- Reloading Nginx... ----------'
nginx -t && systemctl reload nginx
echo '---------- Configuring Certbot to reload Nginx after success renew... ----------'
echo -e '#!/bin/bash\n/usr/sbin/nginx -t && /bin/systemctl restart nginx' | tee /etc/letsencrypt/renewal-hooks/post/nginx-reload.sh
chmod a+x /etc/letsencrypt/renewal-hooks/post/nginx-reload.sh
echo '---------- Setup finished ----------'
